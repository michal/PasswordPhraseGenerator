<!DOCTYPE html>
<html lang="da">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Sætningsgenerator</title>
  <style>
		*{margin:0;padding:0;}html{height:100%;}
  	h1{text-align:center;margin-top:10%;}
  </style>
</head>
<body>
<?php
$tal = rand(2,9);
$model = rand(0,1);
$tegn=array(".","!","?","!!","...","..");
$end=$tegn[rand(0,count($tegn)-1)];
#echo "$model, $tal";
switch($model){
	case 0:
		$adjPl=file("data/adj-e.txt");
		$subUP=file("data/navneord-ubestemt-pluralis.txt");
		$verD =file("data/verber-datid.txt");
		$subBe=file("data/navneord-bestemt.txt");
		$result=$tal." ".$adjPl[rand(0,count($adjPl)-1)]." ".
			$subUP[rand(0,count($subUP)-1)]." ".
			$verD[rand(0,count($verD)-1)]." ".
			$subBe[rand(0,count($subBe)-1)].$end;
		break;
	
	case 1:
		$subUP=file("data/navneord-ubestemt-pluralis.txt");
		$verD =file("data/verber-datid.txt");
		$subUS=file("data/navneord-ubestemt.txt");
		// Fælles- eller intetkøn?
		$adjSN=file("data/adj-n.txt");

		$result=ucwords($adjSN[rand(0,count($adjSN)-1)])." ".
			$subUS[rand(0,count($subUS)-1)]." ".
			$verD[rand(0,count($verD)-1)]." ".
			$tal." ".
			$subUP[rand(0,count($subUP)-1)].$end;
}
$result=str_replace("\n","",$result);

/* 
tal tillæg navneord verb navneord
tillæg navneord verb tal navneord

*/
?><h1><?=$result?></h1>
</body>
</html>
