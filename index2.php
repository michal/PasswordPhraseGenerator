<!DOCTYPE html>
<html lang="da">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Sætningsgenerator</title>
  <style>
		*{margin:0;padding:0;}html{height:100%;}
  	h1{text-align:center;margin-top:10%;}
		span{background: #333;color:#FFF;padding:0.3em;}
  </style>
</head>
<body>
<?php
$tal = rand(2,79);
$model = rand(0,1);
$end=array(".","!","?","!?","..."," :-)"," ;-)");
$tegn=$end[array_rand($end)];

$result = "";

$ord = array(
	"sub"=>JSON_Decode(file_get_contents('data/navneord.json')),
	"ver"=>JSON_Decode(file_get_contents('data/verber.json')),
	"adj"=>JSON_Decode(file_get_contents('data/adj.json')),
	"pro"=>array("jeg","han","hun","den","det","de","I","vi","du","De"),
	"tid"=>array("før","efter","imens","inden"),
	"for"=>array("over","under","ved siden af","bag","foran","overfor"));

function rando($arr){
	return $arr[array_rand($arr)];
}

function word($cla,$conj=null){
	global $ord;
	$word = $ord[$cla][array_rand($ord[$cla])]; //rand(0,count($ord[$cla]));
	if($conj==null){
		return $word;
	}else{
		return $word[$conj];
	}
}

function lego($conj=0){
	// 0 = ubestemt ental, eks. "en grim tyr" el. "et dumt æg".
	// 1 = bestemt ental, eks. "den grimme tyr" el. "det dumme æg"
	// 2 = ubestemt flertal, eks. "ægte tedåser"
	// 3 = bestemt flertal, eks. "de rådne æbler"
	$main = word("sub");
	$desc = word("adj");
	if($main[4]=="t"){$neutrum=true;}else{$neutrum = false;}

	switch($conj){
		case 0:
			if($neutrum){
				$phrase="et ".word("adj")[1]." ".$main[0];
			}else{
				$phrase="en ".word("adj")[0]." ".$main[0];
			}
			break;
		case 1:
			if($neutrum){
				$phrase="det ".word("adj")[2]." ".$main[0];
			}else{
				$phrase="den ".word("adj")[2]." ".$main[0];
			}
			break;
		case 2:
			$phrase=word("adj")[2]." ".$main[2];
			break;
		case 3:
			$phrase="de ".word("adj")[2]." ".$main[2];
			break;
	}
	return $phrase;
}

switch($model){
	case 0:
		if($tal>1){
			$b = lego(2);
		}else{
			$b = lego(rand(0,1));
		}
		$c = word("ver",rand(1,4));
		$d = word("sub",rando([1,3]));
		if(rand(0,1)>0){
			$which=rand(1,3);
			switch($which){
				case 1:
				$b = mb_strtoupper($b);break;
				case 2:
				$c = mb_strtoupper($c);break;
				case 1:
				$d = mb_strtoupper($d);break;
			}
		}
		$result="$tal $b $c $d$tegn";
		break;
	
	case 1:
		$a = lego(rando([0,1,3]));
		$b = word("ver",rand(1,4));
		if($tal>1){$c = word("sub",2);}else{$c = word("sub",rando([1,3]));}
		$result = "$a $b $tal $c$tegn";
		break;
}
?><h1><span><?=ucwords($result,"|")?></span></h1>
</body>
</html>
