#!/usr/bin/env python3
import random, json

result = []

with open('navneord.csv') as f:
	lines = f.read().splitlines()

x = random.randint(0,len(lines))

for l in lines:
	result.append(l.split(","))
	if result[-1][1][-1] == "t":
		result[-1].append("t")
	else:
		result[-1].append("n")

print(json.dumps(result))

#print(result)